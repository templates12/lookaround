curl -X GET http://127.0.0.1:80/api/

###############  LOGIN ####################
curl -X POST -H "Content-Type: application/json" http://127.0.0.1:80/api/register/ -d '{"firstname":"papa", "secondname":"none", "email":"krozin@gmail.com", "user":"kelsor", "passwd":"1234"}'
curl -X GET http://127.0.0.1/api/activate/Imtyb3ppbkBnbWFpbC5jb20i.XeRFMg.Vn9KeKTJAvfkQgR_cbkb2D1gmzU
curl -X POST http://127.0.0.1:80/api/login/ -d user=0001 -d passwd=0001
curl --user 0001:0001 http://127.0.0.1/api/token/
curl --user admin@foobar.baz:123 http://127.0.0.1/api/token/
export MYTOKEN=`curl --user 0001:0001 http://127.0.0.1/api/token/ | grep '{"token": "' | awk {'print $2'} | awk -F\" '{print $2}'`
curl -H "Authorization: Bearer ${MYTOKEN}" http://127.0.0.1/api/check_token/
curl -s -v -H "Authorization: Bearer ${MYTOKEN}" "http://127.0.0.1/api/file/download?type=photo&&xid=1"

curl -v -X POST -H "Authorization: Bearer ${MYTOKEN}" -H "Content-Type: application/x-shellscript" -F name=get-docker.sh -F 'file=@/home/krozin/get-docker.sh' http://127.0.0.1/api/file/upload/<ticket_number>

curl -v -X POST -H "Authorization: Bearer ${MYTOKEN}" -F name=get-docker.sh -F 'file=@/home/krozin/get-docker.sh' http://127.0.0.1/api/file/upload/938d21a3-2390-4399-9662-6251d254aed1


###############  TICKET ####################
curl -X GET -H "Authorization: Bearer <token>" http://127.0.0.1/api/tickets/
curl -X POST -H "Authorization: Bearer ${MYTOKEN}" -H "Content-Type: application/json" http://127.0.0.1/api/tickets/ -d '{"number":"123456"}'
curl -X POST -H "Authorization: Bearer ${MYTOKEN}" -H "Content-Type: application/json" http://127.0.0.1/api/tickets/byradius -d '{"lat":33.559466, "lon":-117.729488, "radius": 100}'

curl -X POST -H "Authorization: Bearer ${MYTOKEN}" -H "Content-Type: application/json" http://127.0.0.1/api/ticket/assign/<ticket_number>
{"id": 1, "created_on": "2019-12-16T23:01:45.150954", "updated_on": "2019-12-16T23:01:45.150973", "number": "cd100a32-4701-43a3-89f9-4151a4213c44", "status": "TODO", "relese_at": null, "relesed_on": null, "ticket_id": 1, "ticket_number": "bf59514d-6402-4c26-a431-3b5526d5dfd0", "user_id": 1}

curl -X POST -H "Authorization: Bearer ${MYTOKEN}" -H "Content-Type: application/json" http://127.0.0.1/api/ticket/release/<ticket_number>
{"id": 1, "created_on": "2019-12-16T23:01:45.150954", "updated_on": "2019-12-16T23:01:45.150973", "number": "cd100a32-4701-43a3-89f9-4151a4213c44", "status": "RELEASED", "relese_at": null, "relesed_on": null, "ticket_id": 1, "ticket_number": "bf59514d-6402-4c26-a431-3b5526d5dfd0", "user_id": 1}

curl -X POST -H "Authorization: Bearer ${MYTOKEN}" -H "Content-Type: application/json" http://127.0.0.1/api/ticket/run/<ticket_number>
curl -X POST -H "Authorization: Bearer ${MYTOKEN}" -H "Content-Type: application/json" http://127.0.0.1/api/ticket/complete/<ticket_number>

############# ML ###############################
curl -X GET -H "Authorization: Bearer $MYTOKEN" http://127.0.0.1/api/ml/run/629f8cc3-2780-4f20-9ee8-51ddf821b1df?xid=1
