import datetime
import uuid
import base64
import json

import flask
from flask_restful import reqparse
import flask_restful
from flask import request, redirect, url_for, flash
from itsdangerous import URLSafeTimedSerializer
from werkzeug.utils import secure_filename

import server
from server import config
from server  import handlers  # noqa
from server import models
from server import helpers
from server import tasks
from server.instances import api
from server.instances import basic_auth
from server.instances import token_auth
from server.logger import logger


def _405():
    flask_restful.abort(
        405, message='The method is not allowed for the requested URL.')


def _406():
    flask_restful.abort(
        406, message='params are not valid')


def _409():
    flask_restful.abort(
        409, message='Conflict')


class BaseListResource(flask_restful.Resource):

    MODEL = None
    POST_ENABLED = True
    PARSER = reqparse.RequestParser()

    def get(self):
        args = self.PARSER.parse_args(strict=True)
        return [obj.to_dict()
                for obj in self.MODEL.query.filter_by(**args).all()]

    @token_auth.login_required
    def post(self):
        if not self.POST_ENABLED:
            _405()
        args = self.PARSER.parse_args(strict=True)
        obj = self.MODEL.from_dict(args)
        obj.save()
        return obj.to_dict(), 201


class BaseResource(flask_restful.Resource):

    MODEL = None
    GET_ENABLED = True
    PUT_ENABLED = True
    DELETE_ENABLED = True

    def get(self, obj_id):
        if not self.GET_ENABLED:
            _405()
        return self.MODEL.query.get_or_404(obj_id).to_dict()

    @token_auth.login_required
    def put(self, obj_id):
        if not self.PUT_ENABLED:
            _405()
        obj = self.MODEL.query.get_or_404(obj_id)
        args = self.PARSER.parse_args(strict=True)
        obj.update_from_dict(args)
        obj.save()
        return obj.to_dict(), 200

    @token_auth.login_required
    def delete(self, obj_id):
        if not self.DELETE_ENABLED:
            _405()
        obj = self.MODEL.query.get_or_404(obj_id)
        self.on_delete(obj)
        obj.delete()
        return '', 204

    def on_delete(self, obj):
        pass


@basic_auth.verify_password
def verify_password(username, password):
    flask.g.user = None
    user = models.User.query.filter_by(username=username).first()
    if not user or not user.verify_password(password):
        return False
    flask.g.user = user
    return True


@token_auth.verify_token
def verify_token(token):
    flask.g.user = None
    user = models.User.verify_auth_token(token)
    if not user:
        return False
    flask.g.user = user
    return True


def generate_confirmation_token(email):
    serializer = URLSafeTimedSerializer(config.SECRET_KEY2)
    return serializer.dumps(email, salt=config.SECURITY_PASSWORD_SALT)


def confirm_token(token, expiration=3600):
    serializer = URLSafeTimedSerializer(config.SECRET_KEY2)
    try:
        email = serializer.loads(token, salt=config.SECURITY_PASSWORD_SALT, max_age=expiration)
    except:
        return False
    return email


@api.resource('/api/')
class ApiResource(flask_restful.Resource):

    def get(self):
        return {'version': server.__version__}


@api.resource('/api/register/')
class Register(flask_restful.Resource):
    PARSER = reqparse.RequestParser()
    PARSER.add_argument('firstname', type=str)
    PARSER.add_argument('secondname', type=str)
    PARSER.add_argument('email', type=str)
    PARSER.add_argument('user', type=str)
    PARSER.add_argument('passwd', type=str)

    def post(self):
        args = self.PARSER.parse_args()
        firstname, secondname = args.get('firstname'), args.get('secondname')
        user, passwd = args.get('user'), args.get('passwd')
        email = args.get('email')
        
        print(args)

        if not firstname or \
           not secondname or \
           not user or not passwd or\
           not email:
            return _406()
        
        obj = models.User.get_or_none(email=email)
        if obj:
            return _409()

        user = models.User(
            firstname=firstname,
            secondname=secondname,
            email=email,
            username=user,
            password=passwd,
            confirmed=False
        )
        user.save()
        token = generate_confirmation_token(user.email)
        confirm_url = 'Please open url in order to activate your account: http://127.0.0.1/api/activate/{}'.format(token)
        subject = "Please confirm your email"
        helpers.send_email(user.email, subject, confirm_url)
        #flask.g.user = user
        return {'message': 'ok', 'token': token}, 201


@api.resource('/api/activate/<token>')
class Activate(flask_restful.Resource):

    def get(self, token):
        print(token)
        email = confirm_token(token)
        user = models.User.get_or_none(email=email)
        if not user:
            return _406()

        if user.confirmed:
            return {'message': 'Account already confirmed. Please login.'}, 200
        user.confirmed = True
        user.confirmed_on = datetime.datetime.now()
        user.save()
        return {'message': 'ok', 'token': token}, 200


@api.resource('/api/login/')
class Login(flask_restful.Resource):

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('user', type=str)
        parser.add_argument('passwd', type=str)
        args = parser.parse_args()
        user, passwd = args.get('user'), args.get('passwd')
        if not verify_password(user, passwd):
            return _406()
        token = flask.g.user.generate_auth_token()
        return {'token': token.decode('ascii'),
                'duration': config.TOKEN_EXPIRES}


@api.resource('/api/token/')
class ApiTokenResource(flask_restful.Resource):

    @basic_auth.login_required
    def get(self):
        token = flask.g.user.generate_auth_token()
        return {'token': token.decode('ascii'),
                'duration': config.TOKEN_EXPIRES}


@api.resource('/api/check_token/')
class ApiTokenTestResource(flask_restful.Resource):

    @token_auth.login_required
    def get(self):
        return {'message': 'token is valid'}


@api.resource('/api/user/newpasswd')
class NewPinResource(BaseResource):
    MODEL = models.Ticket
    POST_ENABLED = True
    DELETE_ENABLED = False
    PUT_ENABLED = False
    GET_ENABLED = False
    PARSER = reqparse.RequestParser()
    PARSER.add_argument('old', type=str)
    PARSER.add_argument('new', type=str)

    @token_auth.login_required
    def post(self):
        if not self.POST_ENABLED:
            _405()
        user = flask.g.user
        args = self.PARSER.parse_args(strict=False)
        old = args.get('old', '')
        new = args.get('new', '')
        if not old:
            return {'message': 'old is empty'}, 404
        if not new:
            return {'message': 'new is empty'}, 404
        if not user.verify_password(old):
            return {'message': 'old is wrong'}, 405
        user.password = new
        user.save()
        return {'message': 'new is applied'}, 201


@api.resource('/api/user/resetpasswd')
class ResetPinResource(BaseResource):
    MODEL = models.Ticket
    POST_ENABLED = True
    DELETE_ENABLED = False
    PUT_ENABLED = False
    GET_ENABLED = False
    PARSER = reqparse.RequestParser()
    PARSER.add_argument('new', type=str)

    # TODO: we shall generate a special reset token
    # and validate this token before
    def post(self):
        if not self.POST_ENABLED:
            _405()
        # GET USER BY SPECIAL RESET TOKEN FROM CACHE
        #user = flask.g.user
        args = self.PARSER.parse_args(strict=False)
        new = args.get('new', '')
        if not new:
            return {'message': 'new is empty'}, 404
        #user.password = new
        #user.save()
        return {'message': 'new is applied'}, 201


########################### TICKET API ##############################

@api.resource('/api/tickets/')
class TicketListResource(BaseListResource):
    MODEL = models.Ticket
    POST_ENABLED = True
    DELETE_ENABLED = False
    PARSER = reqparse.RequestParser()
    PARSER.add_argument('number', type=str)

    @token_auth.login_required
    def post(self):
        if not self.POST_ENABLED:
            _405()
        args = self.PARSER.parse_args(strict=False)
        number = args.get('number')
        ticket = self.MODEL.query.filter_by(number=number).one_or_none()
        if not ticket:
            obj = self.MODEL.from_dict(args)
            obj.save()
            return obj.to_dict(), 201
        return ticket.to_dict(), 201


@api.resource('/api/tickets/byradius')
class TicketListByPointResource(BaseListResource):
    MODEL = models.Ticket
    POST_ENABLED = True
    DELETE_ENABLED = False
    PARSER = reqparse.RequestParser()
    PARSER.add_argument('lat', type=float)
    PARSER.add_argument('lon', type=float)
    PARSER.add_argument('radius', type=float)

    @token_auth.login_required
    def post(self):
        if not self.POST_ENABLED:
            _405()
        args = self.PARSER.parse_args(strict=False)
        lat = args.get('lat')
        lon = args.get('lon')
        radius = args.get('lon')
        objs = helpers.get_points_by_radius(lat, lon, radius, self.MODEL)
        logger.info(objs)
        l = [i.to_dict() for i in objs]
        return l, 200


@api.resource('/api/ticket/<int:obj_id>')
class TicketResource(BaseResource):
    MODEL = models.Ticket
    POST_ENABLED = True
    DELETE_ENABLED = False
    PARSER = models.Ticket.get_fields_parser()


@api.resource('/api/ticket/assign/<ticket_number>')
class TicketAssignResource(BaseResource):
    MODEL = models.Ticket
    POST_ENABLED = True
    DELETE_ENABLED = False
    PARSER = models.Ticket.get_fields_parser()

    @token_auth.login_required
    def post(self, ticket_number):
        if not self.POST_ENABLED:
            _405()
        user = flask.g.user
        ticket = models.Ticket.query.filter_by(number=ticket_number).filter_by(status="TODO").one_or_none()
        if not ticket:
            return 404
        ticket.status = "ASSIGNED"
        ticket.save()

        task = models.Task(
            status="TODO",
            ticket_id=ticket.id,
            ticket = ticket,
            ticket_number = ticket_number,
            user_id=user.id,
            user=user
        )
        task.save()

        return task.to_dict(), 200


@api.resource('/api/ticket/release/<ticket_number>')
class TicketReleaseResource(BaseResource):
    MODEL = models.Ticket
    POST_ENABLED = True
    DELETE_ENABLED = False
    PARSER = models.Ticket.get_fields_parser()

    @token_auth.login_required
    def post(self, ticket_number):
        if not self.POST_ENABLED:
            _405()
        user = flask.g.user
        ticket = models.Ticket.query.filter_by(number=ticket_number).filter_by(status="ASSIGNED").one_or_none()
        if not ticket:
            return 404

        task = models.Task.query.filter_by(user_id=user.id).filter_by(ticket_number=ticket_number).filter_by(status="TODO").one_or_none()
        if not task:
            return 404
        
        task.status = "RELEASED"
        task.relese_at = datetime.datetime.now()
        task.save()
        ticket.status = "TODO"
        ticket.save()
        return task.to_dict(), 200


@api.resource('/api/ticket/run/<ticket_number>')
class TicketRunResource(BaseResource):
    MODEL = models.Ticket
    POST_ENABLED = True
    DELETE_ENABLED = False
    PARSER = models.Ticket.get_fields_parser()

    @token_auth.login_required
    def post(self, ticket_number):
        if not self.POST_ENABLED:
            _405()
        user = flask.g.user
        ticket = models.Ticket.query.filter_by(number=ticket_number).filter_by(status="ASSIGNED").one_or_none()
        if not ticket:
            return 404

        task = models.Task.query.filter_by(user_id=user.id).filter_by(ticket_number=ticket_number).filter_by(status="TODO").one_or_none()
        if not task:
            return 404
        
        task.status = "INPROGRESS"
        task.save()
        ticket.status = "INPROGRESS"
        ticket.save()
        return task.to_dict(), 200


# for debug purpose.
# later ML shall touch MQ client which shall chnage DB
@api.resource('/api/ticket/complete/<ticket_number>')
class TicketCompleteResource(BaseResource):
    MODEL = models.Ticket
    POST_ENABLED = True
    DELETE_ENABLED = False
    PARSER = models.Ticket.get_fields_parser()

    @token_auth.login_required
    def post(self, ticket_number):
        if not self.POST_ENABLED:
            _405()
        user = flask.g.user
        ticket = models.Ticket.query.filter_by(number=ticket_number).filter_by(status="INPROGRESS").one_or_none()
        if not ticket:
            return 404

        task = models.Task.query.filter_by(user_id=user.id).filter_by(ticket_number=ticket_number).filter_by(status="INPROGRESS").one_or_none()
        if not task:
            return 404
        
        task.status = "DONE"
        task.save()
        ticket.status = "DONE"
        ticket.save()
        return task.to_dict(), 200

########################### FILES API ##############################
@api.resource('/api/file/download/<ticket_number>')
class ApiDownloadFile(flask_restful.Resource):

    @token_auth.login_required
    def get(self, ticket_number):
        obj_type = request.args.get('type', type=str)
        obj_xid = request.args.get('xid', type=int)
        obj_name = request.args.get('name', type=str)
        logger.debug("ARGS={}".format(request.args))
        res = None
        if obj_type and obj_xid:
            res = helpers.get_file(obj_type=obj_type, xid=obj_xid)
        elif obj_name:
            res = helpers.get_file(filename=obj_name)
        logger.debug(res)
        return res


@api.resource('/api/file/upload/<ticket_number>')
class ApiUploadFile(flask_restful.Resource):

    @token_auth.login_required
    def post(self, ticket_number):
        # check if the post request has the file part
        if 'file' not in request.files:
            return {'message': 'no file'}, 406
            # return redirect(request.url)

        file = request.files['file']
        # if user does not select file
        if file.filename == '':
            return {'message': 'no file name'}, 406

        user = flask.g.user

        ticket = models.Ticket.query.filter_by(number=ticket_number).filter_by(status="ASSIGNED").one_or_none()
        if not ticket:
            return {'error': 'no ticket'}, 404

        task = models.Task.query.filter_by(user_id=user.id).filter_by(ticket_number=ticket_number).filter_by(status="TODO").one_or_none()
        if not task:
            return {'error': 'no task'}, 404

        filename = secure_filename(file.filename)
        logger.debug("FILENAME={}, CONVERTED={}".format(file.filename, filename))
        helpers.save_file(fileobj=file, filename=filename, ticket_number=ticket_number, task_number=task.number)
        # file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        return {'message': 'saved'}, 200


@api.resource('/api/ml/run/<ticket_number>')
class ApiModelRun(flask_restful.Resource):

    @token_auth.login_required
    def get(self, ticket_number):
        obj_xid = request.args.get('xid', type=int)
        logger.debug("ARGS={}".format(request.args))
        res = None
        if obj_xid:
            fobj = helpers.get_file_content(xid=obj_xid)
            res = json.dumps({'content_base64': base64.b64encode(fobj).decode("utf-8")})
            #res = json.dumps({'content_base64': list(fobj)})
        logger.debug(res)

        try:
            tasks.evaluate_model_1.delay(ml_data=res)
        except Exception as e:
            logger.warning("Exception. {}".format(e))
            return {'message': 'not run'}, 501
        return {'message': 'run'}, 200
