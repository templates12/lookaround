import flask
from flask import views

from server.instances import token_auth


class Api(flask.Blueprint):

    def route(self, path):
        def wrapper(class_):
            self.add_url_rule(path, view_func=class_.as_view(class_.__name__))
            return class_
        return wrapper


class ApiError(Exception):
    """Base Api Error."""


class OpenHandler(views.MethodView):
    """Generic handler"""

    def post(self):
        request = flask.request.get_json(force=True, silent=True)
        return_code = 200
        try:
            response = self.handle_request(request)
        except ApiError as e:
            response = {'message': str(e)}
            return_code = 400
        return flask.jsonify(response), return_code

    def handle_request(self):
        pass


class ApiHandler(OpenHandler):
    """Api that requires token."""

    @token_auth.login_required
    def post(self):
        return super(ApiHandler, self).post()
