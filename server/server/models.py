import datetime as dt
import uuid

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext import declarative
from werkzeug import security
import flask
import flask_restful
from flask_restful import reqparse
import itsdangerous
from mongoengine import signals

from server import config
from server.instances import db
from server.instances import mongodb
from server.instances import redis_store
#from geoalchemy2 import Geometry


jwt = itsdangerous.TimedJSONWebSignatureSerializer(
    config.SECRET_KEY, expires_in=config.TOKEN_EXPIRES)
activation_token_gen = itsdangerous.JSONWebSignatureSerializer(
    config.SECRET_KEY2)
reset_password_token = itsdangerous.TimedJSONWebSignatureSerializer(
    config.SECRET_KEY3, expires_in=config.TOKEN_EXPIRES)
email_confirm_token = itsdangerous.JSONWebSignatureSerializer(
    config.SECRET_KEY4)
Base = declarative_base()


def get_current_user():
    return getattr(flask.g, 'user', None)


def get_current_user_id():
    user = get_current_user()
    if user:
        return user.id


def get_current_user_badge():
    user = get_current_user()
    if user:
        return user.badgeid


def generate_uuid():
    return str(uuid.uuid4())


class ModelSerializerMixin(object):
    __table_args__ = {'extend_existing': True}

    READONLY_FIELDS = ()
    HIDDEN_FIELDS = ()
    FIELD_TYPES = {}

    @classmethod
    def from_dict(cls, d):
        obj = cls()
        for key, value in d.items():
            if key in cls.READONLY_FIELDS:
                flask_restful.abort(
                    400, message='{!r} is read-only'.format(key))
            setattr(obj, key, value)
        return obj

    def update_from_dict(self, d):
        for key, value in d.items():
            if key in self.READONLY_FIELDS:
                flask_restful.abort(
                    400, message='{!r} is read-only'.format(key))
            setattr(self, key, value)

    def to_dict(self):
        d = {}
        for column in self.__table__.columns:
            if column.name in self.HIDDEN_FIELDS:
                continue
            attr = getattr(self, column.name)
            if isinstance(attr, dt.datetime):
                attr = attr.isoformat()
            d[column.name] = attr
        return d

    @classmethod
    def get_fields_parser(cls):
        parser = reqparse.RequestParser()
        for column in cls.__table__.columns:
            col_name = column.name
            if col_name in cls.HIDDEN_FIELDS:
                continue
            if col_name in cls.FIELD_TYPES:
                col_type = cls.FIELD_TYPES[col_name]
            else:
                try:
                    col_type = getattr(column.type, 'python_type')
                except NotImplementedError:
                    continue
            if col_type:
                parser.add_argument(col_name, type=col_type,
                                    store_missing=False, location='json')
                if col_type is not list:
                    parser.add_argument(col_name, type=col_type,
                                        store_missing=False, location='args')
        return parser


class CRUDMixin(object):
    __table_args__ = {'extend_existing': True}

    @classmethod
    def create(cls, **kwargs):
        instance = cls(**kwargs)
        return instance.save()

    @classmethod
    def get_or_create(cls, **kwargs):
        instance = cls.query.filter_by(**kwargs).one_or_none()
        if not instance:
            instance = cls.create(**kwargs)
        return instance

    @classmethod
    def get_or_none(cls, **kwargs):
        instance = cls.query.filter_by(**kwargs).one_or_none()
        return instance

    def update(self, commit=True, **kwargs):
        for attr, value in kwargs.iteritems():
            setattr(self, attr, value)
        return commit and self.save() or self

    def save(self, commit=True):
        db.session.add(self)
        if commit:
            db.session.commit()
        return self

    def delete(self, commit=True):
        db.session.delete(self)
        return commit and db.session.commit()

    def refresh(self):
        db.session.refresh(self)


class BaseModel(db.Model, Base, CRUDMixin, ModelSerializerMixin):
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    created_on = db.Column(db.DateTime, default=dt.datetime.utcnow)
    updated_on = db.Column(db.DateTime, default=dt.datetime.utcnow,
                           onupdate=dt.datetime.utcnow)

    @declarative.declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()


class User(BaseModel):

    firstname = db.Column(db.String(128), nullable=False)
    secondname = db.Column(db.String(128), nullable=False)
    email = db.Column(db.String, unique=True, nullable=False)    
    username = db.Column(db.String(128), unique=True, nullable=False)
    password_hash = db.Column(db.String(256), nullable=False)
    # none/registered/activated/blocked
    status = db.Column(db.String(10), default='none')
    confirmed = db.Column(db.Boolean, nullable=False, default=False)
    confirmed_on = db.Column(db.DateTime, nullable=True)


    @property
    def password(self):
        return None

    @password.setter
    def password(self, password):
        self.password_hash = security.generate_password_hash(password)

    def verify_password(self, password):
        return security.check_password_hash(self.password_hash, password)

    def generate_auth_token(self):
        return jwt.dumps({'id': self.id})

    @staticmethod
    def verify_auth_token(token):
        try:
            data = jwt.loads(token)
        except itsdangerous.SignatureExpired:
            return None    # valid token, but expired
        except itsdangerous.BadSignature:
            return None    # invalid token
        user = User.query.get(data['id'])
        return user


class AdminUser(BaseModel):

    username = db.Column(db.String(128), unique=True, nullable=False)
    password_hash = db.Column(db.String(256), nullable=False)

    @property
    def password(self):
        return None

    @password.setter
    def password(self, password):
        self.password_hash = security.generate_password_hash(password)

    def verify_password(self, password):
        return security.check_password_hash(self.password_hash, password)

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    # Required for administrative interface
    def __unicode__(self):
        return self.username


class Ticket(BaseModel):
    number = db.Column(db.String(50), unique=True, default=generate_uuid)
    # TODO, ASSIGNED, INPROGRESS, DONE, CLOSED
    status = db.Column(db.String(20))
    lat = db.Column(db.Float)
    lon = db.Column(db.Float)
    description = db.Column(db.String(1024))
    priority = db.Column(db.Integer)
    cost = db.Column(db.Float)
    #location = db.Column(Geometry('POINT'))

    def __str__(self):
        return self.number

    def __unicode__(self):
        return self.number


class Task(BaseModel):
    number = db.Column(db.String(50), unique=True, default=generate_uuid)
    # TODO, INPROGRESS, DONE, RELEASED (in case of long assigment)
    status = db.Column(db.String(20))
    relese_at = db.Column(db.DateTime, nullable=True)
    done_on = db.Column(db.DateTime, nullable=True)

    ticket_id = db.Column(db.Integer, db.ForeignKey('ticket.id'))
    ticket_number = db.Column(db.String(50))
    ticket = db.relationship('Ticket')
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User')

    def __str__(self):
        return self.number

    def __unicode__(self):
        return self.number


#tickets_users = db.Table(
#    'tickets_users',
#    db.Column('ticket_id', db.Integer(), db.ForeignKey('ticket.id')),
#    db.Column('user_id', db.Integer(), db.ForeignKey('user.id')))

# ----------- MONGODB ----------------------------
def handler(event):
    """ Signal decorator to allow use of callback
        functions as class decorators
    """

    def decorator(fn):
        def apply(cls):
            event.connect(fn, sender=cls)
            return cls
        fn.apply = apply
        return fn
    return decorator


@handler(signals.pre_save)
def update_modified(sender, document):
    document.updated = dt.datetime.utcnow()
    if hasattr(document, 'genid'):
        if not document.genid:
            document.genid = uuid.uuid4()


class BaseDocument(mongodb.Document):
    meta = {'abstract': True}
    created = mongodb.DateTimeField(default=dt.datetime.utcnow)
    updated = mongodb.DateTimeField()

    @classmethod
    def get_or_none(cls, **kwargs):
        try:
            return cls.objects.get(**kwargs)
        except cls.DoesNotExist:
            return None


@update_modified.apply
class Files(BaseDocument):
    genid = mongodb.StringField(required=True, unique=True, null=False,
                           max_length=100, default=generate_uuid)
    obj_type = mongodb.StringField(choices=config.OBJ_TYPES)
    obj_xid = mongodb.IntField(required=False, min_value=1, max_value=10000)
    fileobj = mongodb.FileField()
    filename = mongodb.StringField(required=False, max_length=256)
    details = mongodb.DictField(required=False)
    ticket_number = mongodb.StringField(required=False, max_length=50)
    task_number = mongodb.StringField(required=False, max_length=50)

    def __str__(self):
        return '{}_{}_{}'.decode('utf-8').format(
            self.genid, self.filename, self.obj_type)
