import flask
import flask_httpauth
import flask_migrate
import flask_restful
import flask_sqlalchemy
import flask_mail
import flask_mongoengine
from flask_redis import FlaskRedis
import celery
from server import config
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


app = flask.Flask(__name__)
app.config.from_pyfile('config.py')
app.config['MONGODB_SETTINGS'] = {
    'db': config.MONGODB_DB,
    'host': config.MONGODB_HOST,
    'port': config.MONGODB_PORT,
    'username': config.MONGODB_USERNAME,
    'password': config.MONGODB_PASSWORD,
    'connect': False,
    'authentication_source': 'admin'
}

basic_auth = flask_httpauth.HTTPBasicAuth()
token_auth = flask_httpauth.HTTPTokenAuth()
db = flask_sqlalchemy.SQLAlchemy(app)
migrate = flask_migrate.Migrate(app, db, directory='server/migrations')
api = flask_restful.Api(app)
mongodb = flask_mongoengine.MongoEngine()
mongodb.init_app(app)
mail = flask_mail.Mail(app)
redis_store = FlaskRedis(app)

#DATABASE_URI = 'postgres+psycopg2://postgres:postgres@localhost:5432/lookaround'
#engine = create_engine(DATABASE_URI)
#Session = sessionmaker(bind=engine)
#session = Session()


def apply_flask_context(celery_app):
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery_app.Task = ContextTask
    return celery_app


celery_app = apply_flask_context(
    celery.Celery('tasks', broker='pyamqp://lookrabbit:2019rabbit1920@rabbit//'))


# basic_auth = flask_httpauth.HTTPBasicAuth()
# token_auth = flask_httpauth.HTTPTokenAuth()
