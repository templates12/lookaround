import datetime
import flask_script
import flask_migrate

from server import models
from server.instances import app
from server.instances import db
from server import config

manager = flask_script.Manager(app)

# migrations
manager.add_command('db', flask_migrate.MigrateCommand)


@manager.command
def drop_db():
    """Drops the db tables."""
    db.reflect()
    db.drop_all()


@manager.command
def create_db():
    """Create the db tables."""
    db.create_all()


@manager.command
def add_user(badge, pin):
    user = models.User(username=badge, password=pin)
    db.session.add(user)
    db.session.commit()


@manager.command
def add_admin(username, password):
    admin_user = models.AdminUser(username=username, password=password)
    db.session.add(admin_user)
    db.session.commit()


@manager.command
def create_test_db():
    """Create test data in tables."""
    user = models.User(
        firstname='user1_first',
        secondname='user1_second', username='0001', password='0001', email='kxlam@bk.ru')
    ticket1 = models.Ticket(
        status='TODO',
        lat=33.55946124,
        lon=-117.72948682,
        description='ticket1',
        priority=1,
        cost=0.99
    )
    ticket2 = models.Ticket(
        status='ASSIGNED',
        lat=33.5594612353,
        lon=-117.72948682,
        description='ticket2',
        priority=1,
        cost=0.99
    )
    ticket2.save()

    ticket3 = models.Ticket(
        status='INPROGRESS',
        lat=33.5594612353,
        lon=-117.730566847,
        description='ticket3',
        priority=1,
        cost=0.99
    )
    ticket4 = models.Ticket(
        status='DONE',
        lat=33.55856124,
        lon=-117.72948682,
        description='ticket4',
        priority=1,
        cost=0.99
    )
    ticket5 = models.Ticket(
        status='CLOSED',
        lat=33.56036124,
        lon=-117.73056684690215,
        description='ticket5',
        priority=1,
        cost=0.99
    )
    ticket6 = models.Ticket(
        status='TODO',
        lat=33.562047,
        lon=-117.727888,
        description='ticket6',
        priority=1,
        cost=0.99
    )

    ticket7 = models.Ticket(
        status='ASSIGNED',
        lat=33.559490,
        lon=-117.729586,
        description='ticket7',
        priority=1,
        cost=1.99
    )
    ticket7.save()

    task1 = models.Task(
        status="TODO",
        ticket_id=ticket2.id,
        ticket = ticket2,
        ticket_number = ticket2.number,
        user_id=user.id,
        user=user
    )

    task2 = models.Task(
        status="TODO",
        ticket_id=ticket7.id,
        ticket = ticket7,
        ticket_number = ticket7.number,
        user_id=user.id,
        user=user
    )

    db.session.add(user)
    db.session.add(ticket1)
    db.session.add(ticket2)
    db.session.add(ticket3)
    db.session.add(ticket4)
    db.session.add(ticket5)
    db.session.add(ticket6)
    db.session.add(ticket7)    
    db.session.add(task1)
    db.session.add(task2)    
    db.session.commit()


@manager.command
def dropmongodb(dbname='lookaround'):
    """Drop mongodb"""
    from pymongo import MongoClient
    from mongoengine import errors, connect
    try:
        client = MongoClient('{}:{}'.format(config.MONGODB_HOST, config.MONGODB_PORT))
        client.drop_database(dbname)
    except:  # noqa
        try:
            connect(host=config.MONGODB_HOST).drop_database(dbname)
        except:  # noqa
            click.secho('Problem during Dropping mongodb', fg='red')
    click.secho('Dropped mongodb', fg='green')


def main():
    manager.run()


if __name__ == '__main__':  # pragma: no cover
    main()
