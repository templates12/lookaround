import click

from server import api  # noqa
from server import instances
from server.instances import app
from server import admin  # noqa
from server import handlers  # noqa


@click.command()
@click.option('--debug', '-d', is_flag=True, help='Enable debug mode')
def main(debug):
    app.run(host='0.0.0.0', debug=debug)


if __name__ == '__main__':  # pragma: no cover
    main()
