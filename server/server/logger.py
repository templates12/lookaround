#!/usr/bin/env python
# -*- coding: utf-8 -*-


import datetime
import logging
import os
import yaml

LOG2DB = False

try:
    import peewee
    LOG2DB = os.getenv('PYLOG2DB', True)
except Exception as e:
    print("peewee was not found. {}".format(e.error()))
    LOG2DB = False


__author__ = 'kirill.rozin@gmail.com'
__version__ = '2.0.1'

LOGGERNAME = str(os.getenv('PYCOMMLOGGER', 'CommLogger'))
CONFIG = str(os.getenv('PYCOMMLOGGERCONFIG', 'logging.yaml'))
LOGLEVEL = os.getenv('PYLOGLEVEL', logging.DEBUG)
PACKAGE_DIR = os.path.dirname(os.path.abspath(__file__))


RESULTS_FOLDER = str(os.getenv(
    'PYCOMMLOGGERDIR', os.path.normpath(PACKAGE_DIR)))
LOGDIR = os.path.join(RESULTS_FOLDER, 'logs')
if not os.path.exists(LOGDIR):
    os.mkdir(LOGDIR)

LOGPATH = os.path.normpath(
    os.path.join(LOGDIR, '{}.log'.format(LOGGERNAME)))
DBPATH = os.path.normpath(
    os.path.join(LOGDIR, '{}.db'.format(LOGGERNAME)))


def get_logger(name, logpath, dbpath, log2db):
    """ DB shall be created upfront but FOLDER for logs
        and db is passed through get_logger.
        In this case we have to keep all entities like
        LogRecord and LogDBHandler inside because
        if peewee is not found or log2db is false then
        we shall not log to DB
    """
    if log2db:
        DATABASE = peewee.SqliteDatabase(dbpath)

        class LogRecord(peewee.Model):
            class Meta:
                database = DATABASE
                table_name = 'logs'
            id = peewee.IntegerField(unique=True, primary_key=True)
            log_level = peewee.IntegerField()
            log_levelname = peewee.TextField()
            log = peewee.TextField()
            created_at = peewee.DateTimeField()
            created_by = peewee.TextField()

            def to_dict(self):
                return {
                    'id': self.id,
                    'log_level': self.log_level,
                    'log_levelname': self.log_levelname,
                    'log': self.log,
                    'created_at': self.created_at,
                    'created_by': self.created_by
                }

        def create_models(dbpath):
            DATABASE.connect()
            DATABASE.create_tables([LogRecord])

        class LogDBHandler(logging.Handler):
            '''
            Customized logging handler that puts logs to the database.
            '''
            def __init__(self):
                logging.Handler.__init__(self)

            def emit(self, record):
                LogRecord.create(
                    log_level=record.levelno,
                    log_levelname=record.levelname,
                    # log=record.msg.strip().replace('\'', '\'\''),
                    log=self.format(record),
                    created_at=datetime.datetime.now(),
                    created_by=record.name)

    if os.path.exists(CONFIG) and os.path.isfile(CONFIG):
        with open(CONFIG, 'rt') as f:
            yaml_conf = yaml.safe_load(f.read())
            logging.config.dictConfig(yaml_conf)
        logger = logging.getLogger(name)
    else:
        logger = logging.getLogger(name)
        stream_handler = logging.StreamHandler()
        file_handler = logging.FileHandler(logpath, encoding='utf8')
        db_handler = None
        formatter = logging \
            .Formatter('[%(asctime)s][%(processName)s %(process)-6d]'
                       '[%(levelname)-8s][%(funcName)-20s] '
                       '%(message)s')
        if (log2db):
            create_models(dbpath)
            db_handler = LogDBHandler()
            db_handler.setFormatter(formatter)
            logger.addHandler(db_handler)
        stream_handler.setFormatter(formatter)
        file_handler.setFormatter(formatter)
        logger.addHandler(stream_handler)
        logger.addHandler(file_handler)
        logger.setLevel(LOGLEVEL)
    return logger


logger = get_logger(LOGGERNAME, LOGPATH, DBPATH, LOG2DB)


if __name__ == '__main__':
    logger.debug('DEBUG')
    logger.info('INFO')
    logger.warning('WARNING')
    logger.error('ERROR')
    logger.critical('CRITICAL')
