import os

# flask configuration
SECRET_KEY = 'default_secrete'
SECRET_KEY2 = 'activation_secrete'
SECRET_KEY3 = 'reset_secrete'
SECRET_KEY4 = 'reset_email_secrete'
SECURITY_PASSWORD_SALT = 'salt_passwd_look'
SQLALCHEMY_DATABASE_URI = os.environ.get(
    'SQLALCHEMY_DATABASE_URI', 'sqlite:////tmp/server.db')
SQLALCHEMY_TRACK_MODIFICATIONS = False

# application configuration
TOKEN_EXPIRES = 3600

# mail settings
MAIL_SERVER = 'smtp.gmail.com'
MAIL_PORT = 465
MAIL_USE_TLS = False
MAIL_USE_SSL = True

# gmail authentication
MAIL_USERNAME = os.environ['APP_MAIL_USERNAME']
MAIL_PASSWORD = os.environ['APP_MAIL_PASSWORD']
MAIL_DEFAULT_SENDER = 'kelsor19@gmail.com'

# mail settings
TEST_MAIL_SERVER = 'mailhog'
TEST_MAIL_PORT = 1025
TEST_MAIL_USE_TLS = False
TEST_MAIL_USE_SSL = False

# gmail authentication
TEST_MAIL_USERNAME = ''
TEST_MAIL_PASSWORD = ''
TEST_MAIL_DEFAULT_SENDER = 'kelsor19@gmail.com'

# mongodb
MONGODB_HOST = os.environ.get('MONGODB_HOST', 'mongodb')
MONGODB_PORT = int(os.environ.get('MONGODB_PORT', 27017))
MONGODB_DB = os.environ.get('MONGODB_DATABASE', 'lookaround')
MONGODB_USERNAME = os.environ.get('MONGODB_USER', 'root')
MONGODB_PASSWORD = os.environ.get('MONGODB_PASS', 'root123')

# redis
REDIS_URL = "redis://redis:6379/0"

# objects
OBJ_TYPES = ['photo']
