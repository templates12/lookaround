import datetime as dt

import flask
import flask_admin
import flask_login
from flask_admin import Admin
from flask_admin import helpers as admin_helpers
from flask_admin.contrib.sqla import ModelView
from flask_admin.contrib.mongoengine import ModelView as ModelMongoView
from wtforms import form
from wtforms import fields
from wtforms import validators
from wtforms.fields import PasswordField

from server import models
from server.instances import app
from server.instances import db
from server.instances import mongodb
from server import helpers


class LoginForm(form.Form):
    login = fields.StringField(validators=[validators.required()])
    password = fields.PasswordField(validators=[validators.required()])

    def validate_login(self, field):
        user = self.get_user()

        if user is None:
            raise validators.ValidationError('Invalid user')

        if not user.verify_password(self.password.data):
            raise validators.ValidationError('Invalid password')

    def get_user(self):
        return db.session.query(models.AdminUser).filter_by(
            username=self.login.data).first()


class AdminIndexView(flask_admin.AdminIndexView):

    @flask_admin.expose('/')
    def index(self):
        if not flask_login.current_user.is_authenticated:
            return flask.redirect(flask.url_for('.login_view'))
        return super(AdminIndexView, self).index()

    @flask_admin.expose('/login/', methods=('GET', 'POST'))
    def login_view(self):
        form = LoginForm(flask.request.form)
        if admin_helpers.validate_form_on_submit(form):
            user = form.get_user()
            flask_login.login_user(user)

        if flask_login.current_user.is_authenticated:
            return flask.redirect(flask.url_for('.index'))
        self._template_args['form'] = form
        return super(AdminIndexView, self).index()

    @flask_admin.expose('/logout/')
    def logout_view(self):
        flask_login.logout_user()
        return flask.redirect(flask.url_for('.index'))

    @flask_admin.expose('/get_report/')
    def get_report(self):
        daterande = flask.request.args.get('daterange')
        start, end = daterande.split(' - ')
        date_format = '%Y/%m/%d %H:%M'
        start = dt.datetime.strptime(start, date_format)
        end = dt.datetime.strptime(end, date_format)

        protocols = models.ProductionProtocol.query.filter(
            models.ProductionProtocol.start >= start,
            models.ProductionProtocol.end <= end)

        f = helpers.generate_report(protocols)
        return flask.send_file(
            f,
            as_attachment=True,
            attachment_filename=f'Report for {daterande}.csv',
            mimetype='text/csv')


login_manager = flask_login.LoginManager()
login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id):
    return db.session.query(models.AdminUser).get(user_id)


admin = Admin(app, 'LookAround',
              index_view=AdminIndexView(),
              base_template='my_master.html',
              template_mode='bootstrap3')


def admin_view(cls):
    admin.add_view(cls(cls.model_cls, db.session))
    return cls


def admin_mongoview(cls):
    admin.add_view(cls(cls.model_cls))
    return cls


class BaseModelView(ModelView):
    model_cls = None
    form_excluded_columns = ['created_on', 'updated_on']

    def is_accessible(self):
        return flask_login.current_user.is_authenticated

    def _handle_view(self, name, **kwargs):
        """ Override builtin _handle_view in order to redirect
            users when a view is not accessible.
        """
        if not self.is_accessible():
            if flask_login.current_user.is_authenticated:
                # permission denied
                flask.abort(403)
            else:
                # login
                return flask.redirect(flask.url_for(
                    'admin.logout_view', next=flask.request.url))


class BaseMongoModel(ModelMongoView):
    model_cls = None
    form_excluded_columns = ['created_on', 'updated_on']

    def is_accessible(self):
        return flask_login.current_user.is_authenticated

    def _handle_view(self, name, **kwargs):
        """ Override builtin _handle_view in order to redirect
            users when a view is not accessible.
        """
        if not self.is_accessible():
            if flask_login.current_user.is_authenticated:
                # permission denied
                flask.abort(403)
            else:
                # login
                return flask.redirect(flask.url_for(
                    'admin.logout_view', next=flask.request.url))    


@admin_view
class UserView(BaseModelView):
    model_cls = models.User

    column_exclude_list = (
        'password',
        'password_hash',
    )
    form_extra_fields = {
        'password': PasswordField('Password', [validators.required()])
    }
    form_columns = (
        # 'firtname',
        # 'secondname',
        'username',
        'password',
    )


@admin_view
class TicketView(BaseModelView):
    model_cls = models.Ticket

    form_columns = (
        'number',
        'status',
        'lat',
        'lon',
        'priority',
        'cost',
        'description',
    )


@admin_view
class TaskView(BaseModelView):
    model_cls = models.Task

    """form_columns = (
        'number',
        'status',
        'relese_at',
        'relesed_on',
        #'ticket_id',
        'ticket',
        #'user_id',
        'user',
    )"""

@admin_mongoview
class FilesView(BaseMongoModel):
    model_cls = models.Files
    page_size = 100
