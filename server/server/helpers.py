import datetime
import random
from flask import make_response
import smtplib, ssl
import uuid
import mimetypes
import os
import sqlalchemy
from sqlalchemy.sql import operators
import pika
import sys

from server import config
from server import models
from server.logger import logger
from server import kpygeo


mimetypes.init()


def dict_keyis_dates(objs, orderby):
    res = {}
    for obj in objs:
        if obj.get(orderby):
            try:
                dd = datetime.datetime.strptime(
                    obj.get(orderby), '%Y-%m-%dT%H:%M:%S.%f')
            except ValueError:
                dd = datetime.datetime.strptime(
                    obj.get(orderby), '%Y-%m-%dT%H:%M:%S')
            key = '{0:%b} {0:%d}'.format(dd)
            if not res.get(key):
                res[key] = []
            res[key].append(obj)
        else:
            continue
    return res


def get_file(genid=None, obj_type=None, xid=None, filename=None):
    logger.debug("get_file: {} {} {}".format(obj_type, xid, filename))
    if not obj_type and not xid and not filename and not genid:
        return
    if obj_type and xid:
        f_obj = models.Files.objects(obj_type=obj_type, obj_xid=xid).first()
    elif filename:
        f_obj = models.Files.objects(filename=filename).first()
    elif genid:
        f_obj = models.Files.objects(genid=genid).first()
    # logger.debug("get_file: fileobj step1. f_obj={}. name={} fileobj_methods={}".format(f_obj.genid, f_obj.filename, dir(f_obj.fileobj)))
    if not f_obj:
        return {'result': {'success': False,
                           'details': 'id has not been found'}}
    # logger.debug("get_file: fileobj step2. dir(f_obj)={}".format(dir(f_obj)))
    fobj = f_obj.fileobj.read()
    # logger.debug("get_file: fobj={}".format(fobj))

    if not fobj:
        return {'result': {'success': False, 'details': 'file is absent'}}

    response = make_response(fobj)
    response.headers['Content-Type'] = 'application/octet-stream'
    response.headers["Content-Disposition"] = \
        "attachment; filename={}".format(f_obj.filename)
    return response


def save_file(fileobj, filename, ticket_number, task_number, details=None):
    if not fileobj or not filename or not ticket_number or not task_number:
        return
    logger.debug("fileobj methods={}\n fname={} name={}".format(dir(fileobj), fileobj.filename, fileobj.name))
    f = models.Files(genid=str(uuid.uuid4()),
                     obj_type=config.OBJ_TYPES[0],
                     obj_xid=random.randint(0, 10000),
                     # fileobj=fileobj,
                     filename=filename,
                     details=details,
                     ticket_number=ticket_number,
                     task_number=task_number)
    # f.fileobj.put(fileobj, filename=filename, content_type = 'application/x-shellscript')
    n, extension = os.path.splitext(filename)
    f.fileobj.put(fileobj, filename=filename, content_type=mimetypes.types_map[extension])
    f.save()
    return True


def get_file_content(genid=None, xid=None):
    logger.debug("get_file: xid={} genid={} ".format(xid, genid))
    if not xid and not genid:
        return
    if xid:
        f_obj = models.Files.objects(obj_xid=xid).first()
    elif genid:
        f_obj = models.Files.objects(genid=genid).first()
    # logger.debug("get_file: fileobj step1. f_obj={}. name={} fileobj_methods={}".format(f_obj.genid, f_obj.filename, dir(f_obj.fileobj)))
    if not f_obj:
        return
    # logger.debug("get_file: fileobj step2. dir(f_obj)={}".format(dir(f_obj)))
    fobj = f_obj.fileobj.read()
    # logger.debug("get_file: fobj={}".format(fobj))
    if not fobj:
        return
    return fobj


def send_email(to, subject, html, test=True):   
    receiver_email = to
    smtp_server = config.TEST_MAIL_SERVER
    port = config.TEST_MAIL_PORT
    sender_email = config.TEST_MAIL_DEFAULT_SENDER
    password = config.TEST_MAIL_PASSWORD
    if not test:
        smtp_server = config.MAIL_SERVER
        port = config.MAIL_PORT
        sender_email = config.MAIL_DEFAULT_SENDER
        password = config.MAIL_PASSWORD

        # Create a secure SSL context
        context = ssl.create_default_context()

        with smtplib.SMTP_SSL(smtp_server, port, context=context) as mail_server:
            mail_server.ehlo()
            mail_server.login(sender_email, password)
            mail_server.sendmail(sender_email, receiver_email, html)
    else:
        with smtplib.SMTP(host=smtp_server, port=port) as mail_server:
            mail_server.sendmail(sender_email, receiver_email, html)


def get_points_by_radius(la, lo, radius, model):
    tr, tl, dl, dr = kpygeo.find_bbox_by_point(la, lo, radius)
    if la > 0 and lo > 0:
        objs = model.query.filter(model.lat > dl[0], model.lat < tl[0], model.lon > tl[1], model.lon < tr[1]).all()
    elif la > 0 and lo < 0:
        objs = model.query.filter(model.lat > dl[0], model.lat < tl[0], model.lon < tl[1], model.lon > tr[1]).all()
    elif la < 0 and lo < 0:
        objs = model.query.filter(model.lat < dl[0], model.lat > tl[0], model.lon < tl[1], model.lon > tr[1]).all()
    elif la < 0 and lo > 0:
        objs = model.query.filter(model.lat < dl[0], model.lat > tl[0], model.lon > tl[1], model.lon < tr[1]).all()
    #objs = model.query.filter(model.status != 'NA').all()
    return objs


def emit_message(user, passwd, message, host='127.0.0.1', port=5672, exchange='lookaround', routing_key=''):
    credentials = pika.PlainCredentials(user, passwd)
    connection = pika.BlockingConnection(pika.ConnectionParameters(host=host, port=port, credentials=credentials))
    channel = connection.channel()
    channel.exchange_declare(exchange=exchange, exchange_type='fanout')
    channel.basic_publish(exchange=exchange, routing_key='', body=message)
    print(" [x] Sent %r" % message)
    connection.close()


def consume_message(user, passwd, host='127.0.0.1', port=5672, exchange='lookaround', queue='', routing_key=''):
    def callback(ch, method, properties, body):
        print(" [x] %r" % body)
        # return body

    credentials = pika.PlainCredentials(user, passwd)
    connection = pika.BlockingConnection(pika.ConnectionParameters(host=host, credentials=credentials))
    channel = connection.channel()
    channel.exchange_declare(exchange=exchange, exchange_type='fanout')
    result = channel.queue_declare(queue=queue, exclusive=True)
    queue_name = result.method.queue
    channel.queue_bind(exchange=exchange, queue=queue_name)
    print(' [*] Waiting for logs. To exit press CTRL+C')
    channel.basic_consume(queue=queue_name, on_message_callback=callback, auto_ack=True)
    channel.start_consuming()
