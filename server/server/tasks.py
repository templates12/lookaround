import flask
from server.instances import celery_app
from server.logger import logger

@celery_app.task
def add(x, y):
    return x + y


@celery_app.task
def evaluate_model_1(ml_data):
    logger.info("ml_data = {}".format(ml_data))
    return "ok"
