"""Initial

Revision ID: 8a690da7cc57
Revises:
Create Date: 2019-08-20 18:43:18.137543

"""
from alembic import op
import sqlalchemy as sa
from geoalchemy2 import Geometry


# revision identifiers, used by Alembic.
revision = '8a690da7cc57'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'adminuser',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('created_on', sa.DateTime(), nullable=True),
        sa.Column('updated_on', sa.DateTime(), nullable=True),
        sa.Column('username', sa.String(length=128), nullable=False),
        sa.Column('password_hash', sa.String(length=256), nullable=False),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('username')
    )
    op.create_table(
        'user',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('created_on', sa.DateTime(), nullable=True),
        sa.Column('updated_on', sa.DateTime(), nullable=True),
        sa.Column('confirmed_on', sa.DateTime(), nullable=True),
        sa.Column('firstname', sa.String(length=128), nullable=False),
        sa.Column('secondname', sa.String(length=128), nullable=False),
        sa.Column('email', sa.String(length=128), nullable=False),
        sa.Column('username', sa.String(length=128), nullable=False),
        sa.Column('password_hash', sa.String(length=256), nullable=False),
        sa.Column('status', sa.String(length=10), nullable=True),
        sa.Column('confirmed', sa.Boolean()),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('username')
    )    
    op.create_table(
        'ticket',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('created_on', sa.DateTime(), nullable=True),
        sa.Column('updated_on', sa.DateTime(), nullable=True),
        sa.Column('status', sa.String(length=20), nullable=True),
        sa.Column('number', sa.String(length=50), nullable=True),
        sa.Column('lat', sa.Float),
        sa.Column('lon', sa.Float),
        sa.Column('description', sa.String(1024)),
        sa.Column('priority', sa.Integer),
        sa.Column('cost', sa.Integer),
        # sa.Column('location', Geometry('POINT'), table='ticket', schema=None),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('number')
    )
    op.create_table(
        'task',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('created_on', sa.DateTime(), nullable=True),
        sa.Column('updated_on', sa.DateTime(), nullable=True),
        sa.Column('status', sa.String(length=20), nullable=True),
        sa.Column('number', sa.String(length=50), nullable=True),
        sa.Column('ticket_number', sa.String(length=50), nullable=False),
        sa.Column('relese_at', sa.DateTime(), nullable=True),
        sa.Column('done_on', sa.DateTime(), nullable=True),
        sa.ForeignKeyConstraint(['user'], ['user._id'], ),
        sa.ForeignKeyConstraint(['ticket'], ['ticket._id'], ),
        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('number')
    )    
    # op.create_table(
    #    'tickets_users',
    #    sa.Column('ticket_id', sa.Integer(), nullable=True),
    #    sa.Column('user_id', sa.Integer(), nullable=True),
    #    sa.ForeignKeyConstraint(['ticket_id'], ['ticket.id'], ),
    #    sa.ForeignKeyConstraint(['user_id'], ['user.id'], )
    # )


def downgrade():
    raise NotImplementedError()
