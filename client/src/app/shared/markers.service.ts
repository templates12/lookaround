import { Injectable } from '@angular/core';
import * as L from 'leaflet';
import { MapMarker } from './color-icons.enum';

@Injectable({
  providedIn: 'root'
})
export class MarkersService {

  private readonly DEFAUL_ICON_SIZE: [number, number] = [40, 40];

  getMarkerIconOptions(iconUrl: MapMarker, size?: [number, number]): L.MarkerOptions {
    const iconSize = size ? size : this.DEFAUL_ICON_SIZE;
    const icon: L.Icon = new L.Icon({ iconUrl, iconSize });
    return { icon };
  }

  constructor() { }
}
