export enum MapMarker {
    HOT_RED = '../assets/markers/hot-red-marker.svg',
    LIGHT_RED_ICON = '../assets/markers/hot-red-marker.svg',
    MIDDLE_YELLOW_ICON = '../assets/markers/middle-yellow-marker.svg',
    LIGHT_BLUE_ICON = '../assets/markers/light-blue-marker.svg',
    COLD_BLUE_ICON = '../assets/markers/cold-blue-marker.svg'
}
