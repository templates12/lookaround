import { Component, OnInit } from '@angular/core';
import { faUserCircle, faMapMarkerAlt, faQuestionCircle, faSignOutAlt } from '@fortawesome/free-solid-svg-icons';
import { AppState } from 'src/app/reducers';
import { Store } from '@ngrx/store';
import { logout } from 'src/app/auth/store/auth.actions';
import { SidenavService } from '../sidenav.service';

@Component({
  selector: 'app-sidenav-content',
  templateUrl: './sidenav-content.component.html',
  styleUrls: ['./sidenav-content.component.scss']
})
export class SidenavContentComponent implements OnInit {
  // Icons importing for using in HTML
  faUserCircle = faUserCircle;
  faMapMarkerAlt = faMapMarkerAlt;
  faQuestionCircle = faQuestionCircle;
  faSignOutAlt = faSignOutAlt;

  accountBalance = '18.00';
  accountEmail = `johndoe@gmail.com`;

  constructor(private store: Store<AppState>, private sidenavService: SidenavService) { }

  ngOnInit() {
  }

  onAccount() {
    // TO-DO
  }

  onMyTasks() {
    // TO-DO
  }

  onHowTo() {
    // TO-DO
  }

  onLogOut() {
    this.sidenavService.close();
    this.store.dispatch(logout());
  }

}
