import { Component, OnInit, AfterViewInit } from '@angular/core';
import * as L from 'leaflet';
import { MarkersService } from '../shared/markers.service';
import { MapMarker } from '../shared/color-icons.enum';

@Component({
  selector: 'app-leaflet-map',
  templateUrl: './leaflet-map.component.html',
  styleUrls: ['./leaflet-map.component.scss']
})
export class LeafletMapComponent implements OnInit, AfterViewInit {

  constructor(private markersService: MarkersService) { }

  ngOnInit() {

  }

  ngAfterViewInit() {

    const map = new L.Map('map', {
      center: new L.LatLng(51.778316, 19.452209),
      zoom: 15,
    });

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    // Example markers
    const marker1 = new L.Marker([51.778, 19.448], this.markersService.getMarkerIconOptions(MapMarker.HOT_RED));
    const marker2 = new L.Marker([51.778437, 19.449680], this.markersService.getMarkerIconOptions(MapMarker.LIGHT_RED_ICON));
    const marker3 = new L.Marker([51.778497, 19.450570], this.markersService.getMarkerIconOptions(MapMarker.MIDDLE_YELLOW_ICON));
    const marker4 = new L.Marker([51.778543, 19.451439], this.markersService.getMarkerIconOptions(MapMarker.LIGHT_BLUE_ICON));
    const marker5 = new L.Marker([51.778317, 19.452029], this.markersService.getMarkerIconOptions(MapMarker.COLD_BLUE_ICON));

    tiles.addTo(map);
    // Adding all markers on map, binding to event
    L.featureGroup([marker1, marker2, marker3, marker4, marker5])
      .on('click', (leafletEvent: L.LeafletEvent) => this.onMarkerClick(leafletEvent))
      .addTo(map);
  }

  onMarkerClick(leafletEvent: L.LeafletEvent) {
    const markerLatLong: L.LatLng = leafletEvent.sourceTarget._latlng;
    // TO-DO Open Bottom sheet with information
    console.log(markerLatLong);
  }

}
