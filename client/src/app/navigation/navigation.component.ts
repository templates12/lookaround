import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { SidenavService } from '../header/sidenav.service';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements AfterViewInit {

  constructor(public router: Router, private sidenavService: SidenavService) { }

  @ViewChild('sidenav') public sidenav: MatSidenav;

  ngAfterViewInit() {
    this.sidenavService.setSidenav(this.sidenav);
  }

}
