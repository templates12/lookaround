import { Component, OnInit } from '@angular/core';
import { faMapMarked, faList } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  // Icons importing
  faMapMarked = faMapMarked;
  faList = faList;

  constructor() { }

  ngOnInit() {

  }

}
