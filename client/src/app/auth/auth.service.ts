import { of } from 'rxjs';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { tap, catchError, map } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { User } from './models/user.model';
import { AuthState } from './store/auth.reducer';
import * as AuthActions from './store/auth.actions';
import { SignUpForm } from './models/signup-form.model';
import { AuthResponse } from './models/auth-response.model';
import { LoginFormPayload } from './models/login-form.model';
import { SignUpResponse } from './models/signup-response.model';
import { LOGIN_ENDPOINT, SIGNUP_ENDPOINT } from '../shared/backend-endpoints.const';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private tokenExpirationTimer;

  constructor(private httpClient: HttpClient, private store: Store<AuthState>, private router: Router) {
  }

  login(loginForm: LoginFormPayload) {
    return this.httpClient
      .post<AuthResponse>(LOGIN_ENDPOINT, {
        user: loginForm.username,
        passwd: loginForm.password
      })
      .pipe(
        tap((resData: AuthResponse) => this.setLogoutTimer(+resData.duration * 1000)),
        map((resData: AuthResponse) => {
          return this.handleLogin(resData.token, +resData.duration);
        }),
        catchError(errorRes => {
          return this.handleError(errorRes);
        }));
  }

  signUp(signUpForm: SignUpForm) {
    return this.httpClient.post<SignUpResponse>(SIGNUP_ENDPOINT, {
      firstname: signUpForm.firstName,
      secondname: signUpForm.secondName,
      email: signUpForm.email,
      user: signUpForm.username,
      passwd: signUpForm.password
    })
      .pipe(
        map(() => {
          return AuthActions.signUpSuccess();
        }),
        catchError(errorRes => {
          return this.handleError(errorRes);
        }));
  }

  private handleLogin(token: string, expiresIn: number) {
    const expirationDate = new Date(new Date().getTime() + expiresIn * 1000).toJSON();
    const user: User = {
      token,
      tokenExpirationDate: expirationDate
    } as const;

    localStorage.setItem('userData', JSON.stringify(user));
    return AuthActions.loginSuccess({ user });
  }

  logOut() {
    this.clearLogoutTimer();
    localStorage.removeItem('userData');
    this.router.navigateByUrl('/login');
  }

  autoLogin() {

    const userData = JSON.parse(localStorage.getItem('userData'));

    if (!userData) {
      console.log('No stored user data found');
      return AuthActions.autoLoginFail();
    }

    const serializedTime = new Date(userData.tokenExpirationDate);

    const user: User = {
      token: userData.token,
      tokenExpirationDate: serializedTime.toJSON()
    } as const;

    if (user.token) {
      const expirationDuration = serializedTime.getTime() - new Date().getTime();
      this.setLogoutTimer(expirationDuration);
      return AuthActions.loginSuccess({ user });
    }
    return AuthActions.autoLoginFail();
  }

  autoLogout(expirationDuration: number) {
    this.tokenExpirationTimer = setTimeout(() => {
      this.logout();
    }, expirationDuration);
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.router.navigate(['/login']);
    if (this.tokenExpirationTimer) {
      clearTimeout(this.tokenExpirationTimer);
    }
  }

  private handleError(error: HttpErrorResponse) {
    switch (error.status) {
      case (406):
        // The backend returned an error
        console.error(
          `Backend returned code ${error.status}, ` +
          `body was: `, error.error);
        return of(AuthActions.authFail({ errMessage: 'Invalid username or password' }));
      case (409):
        const errMsg = 'User with such username or email already exists';
        console.error(errMsg);
        return of(AuthActions.authFail({ errMessage: errMsg }));
      default:
        // A client-side or network error occurred. Handle it accordingly.
        console.error(`An error occurred: ${error.statusText}`);
        return of(AuthActions.authFail({ errMessage: error.statusText }));
    }
  }

  private setLogoutTimer(expirationDuration: number) {
    this.tokenExpirationTimer = setTimeout(() => {
      this.store.dispatch(AuthActions.logout());
    }, expirationDuration);
  }

  private clearLogoutTimer() {
    if (this.tokenExpirationTimer) {
      clearTimeout(this.tokenExpirationTimer);
      this.tokenExpirationTimer = null;
    }
  }

}
