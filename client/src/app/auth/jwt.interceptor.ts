import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppState } from '../reducers';
import { Store } from '@ngrx/store';
import { take, map, exhaustMap } from 'rxjs/operators';
import { selectAuthState } from './store/auth.selectors';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private store: Store<AppState>) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        return this.store.select(selectAuthState).pipe(
            take(1),
            map(authState => authState.user),
            exhaustMap(user => {
                if (!user) {
                    return next.handle(request);
                }
                const modifiedRequest = request.clone({
                    setHeaders: {
                        Authorization: `Bearer ${user.token}`
                    }
                });
                return next.handle(modifiedRequest);
            })
        );
    }
}
