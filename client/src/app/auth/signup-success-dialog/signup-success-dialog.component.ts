import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-signup-success-dialog',
  templateUrl: './signup-success-dialog.component.html',
  styleUrls: ['./signup-success-dialog.component.scss']
})
export class SignupSuccessDialogComponent {

  constructor(public dialogRef: MatDialogRef<SignupSuccessDialogComponent>) {
  }

  onCloseDialog(): void {
    this.dialogRef.close();
  }
}
