import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTabGroup } from '@angular/material/tabs';
import { SignupSuccessDialogComponent } from './signup-success-dialog/signup-success-dialog.component';
import { Store } from '@ngrx/store';
import { AppState } from '../reducers';
import * as AuthActions from './store/auth.actions';
import { AuthState } from './store/auth.reducer';
import { selectAuthState, isSignedUpSuccess } from './store/auth.selectors';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit, AfterViewInit {
  @ViewChild(MatTabGroup) tabGroup: MatTabGroup;
  tabIndex = 1;
  loginForm: FormGroup;
  signUpForm: FormGroup;

  constructor(
    private snackBar: MatSnackBar,
    public dialog: MatDialog,
    private store: Store<AppState>,
    private fb: FormBuilder) { }

  ngOnInit() {
    this.initForm();
    this.store.select(selectAuthState).subscribe((authState: AuthState) => {
      if (authState.authError) {
        this.openSnackBar(authState.authError, 'OK');
      }
    });

    this.store.select(isSignedUpSuccess)
      .subscribe((isSignedUp: boolean) => {
        if (isSignedUp) {
          this.dialog.open(SignupSuccessDialogComponent);
        }
      });
  }

  ngAfterViewInit() {
    this.tabGroup.selectedIndexChange.subscribe((index: number) => {
      this.tabIndex = index;
    });
    // Switching tab to login if popup about successful signing up was closed
    this.dialog.afterAllClosed.subscribe(() => {
      this.tabGroup.selectedIndex = 1;
    });
  }

  private initForm(): void {

    this.loginForm = this.fb.group({
      username: [null, Validators.required],
      password: [null, Validators.required]
    });

    this.signUpForm = this.fb.group({
      firstName: [null, Validators.required],
      secondName: [null, Validators.required],
      email: [null, [Validators.email, Validators.required]],
      username: [null, Validators.required],
      password: [null, Validators.required]
    });

  }

  onLogin(): void {
    this.store.dispatch(AuthActions.loginStart({ payload: this.loginForm.value }));
  }

  onSignUp(): void {
    this.store.dispatch(AuthActions.signUpStart({ signUpForm: this.signUpForm.value }));
  }

  openSnackBar(message: string, action: string): void {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
