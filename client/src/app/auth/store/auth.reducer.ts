import { MetaReducer, createReducer, on } from '@ngrx/store';
import { environment } from '../../../environments/environment';
import { AuthActions, logout, loginStart, signUpStart, loginSuccess, authFail, signUpSuccess, resetSignUpMsg } from './auth.actions';
import { User } from '../models/user.model';

export const authFeatureKey = 'auth';

export interface AuthState {
  user: User;
  authError: string;
  loading: boolean;
  signUpRequestSent: boolean;
}

export const initialAuthState: AuthState = {
  user: null,
  authError: null,
  loading: false,
  signUpRequestSent: false
} as const;

const authReducer = createReducer(
  initialAuthState,

  on(loginStart, signUpStart, (state, action) => {
    return {
      ...state,
      authError: null,
      loading: true
    };
  }),

  on(loginSuccess, (state, action) => {
    return {
      ...state,
      user: action.user,
      authError: null,
      loading: false
    };
  }),

  on(signUpSuccess, (state, action) => {
    return {
      ...state,
      loading: false,
      authError: null,
      signUpRequestSent: true
    };
  }),

  on(authFail, (state, action) => {
    return {
      ...state,
      user: null,
      authError: action.errMessage,
      loading: false
    };
  }),

  on(logout, (state, action) => {
    return {
      ...state,
      user: null
    };
  }),

  on(resetSignUpMsg, (state, action) => {
    return {
      ...state,
      signUpRequestSent: false
    };
  })
);

export function reducer(state: AuthState | undefined, action: AuthActions) {
  return authReducer(state, action);
}

export const metaReducers: MetaReducer<AuthState>[] = !environment.production ? [] : [];
