import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, ofType, Effect } from '@ngrx/effects';
import { tap, switchMap, map } from 'rxjs/operators';
import { AuthActions, logout, loginSuccess, loginStart, autoLogin, signUpStart, signUpSuccess, resetSignUpMsg } from './auth.actions';
import { AuthService } from '../auth.service';

@Injectable()
export class AuthEffects {

    @Effect()
    login$ = this.actions$
        .pipe(
            ofType(loginStart),
            switchMap(loginAction => this.authService.login(loginAction.payload))
        );

    @Effect()
    signUp$ = this.actions$
        .pipe(
            ofType(signUpStart),
            switchMap(signUpAction => this.authService.signUp(signUpAction.signUpForm))
        );

    @Effect({ dispatch: false })
    logout$ = this.actions$
        .pipe(
            ofType(logout),
            tap(() => this.authService.logOut())
        );

    @Effect()
    autoLogin$ = this.actions$
        .pipe(
            ofType(autoLogin),
            map(() => this.authService.autoLogin())
        );

    @Effect({ dispatch: false })
    loginSuccess$ = this.actions$
        .pipe(
            ofType(loginSuccess),
            tap(() => {
                console.log('User loggen in successfuly');
                this.router.navigate(['/']);
            })
        );

    @Effect()
    signUpSuccess$ = this.actions$
        .pipe(
            ofType(signUpSuccess),
            map(() => {
                console.log('User signed up successfuly');
                return resetSignUpMsg();
            })
        );

    constructor(private actions$: Actions, private router: Router, private authService: AuthService) {

    }
}
