import { createFeatureSelector, createSelector } from '@ngrx/store';
import { AuthState, authFeatureKey } from './auth.reducer';


export const selectAuthState = createFeatureSelector<AuthState>(authFeatureKey);


export const isLoading = createSelector(
    selectAuthState,
    (authState: AuthState) => authState.loading

);

export const isError = createSelector(
    selectAuthState,
    (authState: AuthState) => authState.authError
);

export const isLoggedIn = createSelector(
    selectAuthState,
    (authState: AuthState) => !!authState.user

);

export const isSignedUpSuccess = createSelector(
    selectAuthState,
    (authState: AuthState) => authState.signUpRequestSent
);
