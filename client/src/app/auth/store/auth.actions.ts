import { createAction, props, union } from '@ngrx/store';
import { LoginFormPayload } from '../models/login-form.model';
import { User } from '../models/user.model';
import { SignUpForm } from '../models/signup-form.model';

export const loginStart = createAction(
    '[Auth] User Login',
    props<{ payload: LoginFormPayload }>()
);

export const signUpStart = createAction(
    '[Auth] User Sign Up',
    props<{ signUpForm: SignUpForm }>()
);

export const authFail = createAction(
    '[Auth] Auth Error',
    props<{ errMessage: string }>()
);

export const loginSuccess = createAction(
    '[Auth] User was successfuly authenticated',
    props<{ user: User }>()
);

export const signUpSuccess = createAction(
    '[Auth] User successfuly signed up'
);

export const logout = createAction(
    '[Auth] User Logout'
);

export const autoLogin = createAction(
    '[Auth] Auto Login'
);

export const autoLoginFail = createAction(
    '[Auth] Auto Login failed'
);

export const resetSignUpMsg = createAction(
    '[Auth] Reseting Sign Up Message Flag'
);

const actions = union({
    loginStart,
    loginSuccess,
    signUpStart,
    signUpSuccess,
    resetSignUpMsg,
    authFail,
    logout,
    autoLogin,
    autoLoginFail
});

export type AuthActions = typeof actions;
