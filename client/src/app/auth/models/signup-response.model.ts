export interface SignUpResponse {
    readonly message: string;
    readonly token: string;
}
