export interface SignUpForm {
    firstName: string;
    secondName: string;
    email: string;
    username: string;
    password: string;
}
