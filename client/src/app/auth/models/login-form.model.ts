export interface LoginFormPayload {
    username: string;
    password: string;
}
