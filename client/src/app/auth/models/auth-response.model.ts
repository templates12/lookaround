export interface AuthResponse {
    readonly token: string;
    readonly duration: number;
}
