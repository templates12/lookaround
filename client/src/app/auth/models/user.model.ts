export interface User {
  readonly token: string;
  readonly tokenExpirationDate: string;
}
