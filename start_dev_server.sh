#!/usr/bin/env bash
set -e

MONGO_ROOT_USER=devroot
MONGO_ROOT_PASSWORD=devroot
MONGOEXPRESS_LOGIN=dev
MONGOEXPRESS_PASSWORD=dev

docker-compose down
docker-compose build
docker-compose run server bash -c 'bash -s <<EOF

server_manage drop_db
server_manage create_db
echo ADD_ADMIN
server_manage add_admin admin 123
echo ADD_USER
server_manage create_test_db

EOF'

docker-compose up -d --force-recreate
docker-compose logs -f
